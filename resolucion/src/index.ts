enum Familias { Lannister, Targaryen, ASaber }
type EstadoVital = "vivo" | "muerto";
type Destreza = 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10;

interface Persona {
    nombre: string;
    familia: Familias;
    edad: number;
    estado: EstadoVital;
    comunicar(): void;
}

class Luchador implements Persona {

    public arma: string;
    public destreza: Destreza;

    constructor(public nombre: string, public familia: Familias, public edad: number, public estado: EstadoVital) { }

    comunicar(): void {
        console.log("Primero pego y luego pregunto");
    }

}

class Asesor implements Persona {

    public asesorado: Persona;

    constructor(public nombre: string, public familia: Familias, public edad: number, public estado: EstadoVital) { }

    comunicar(): void {
        console.log('No sé por qué, pero creo que voy a morir pronto');
    }
}

class Escudero implements Persona {

    constructor(public nombre: string, public familia: Familias, public edad: number, public estado: EstadoVital) { }

    comunicar() {
        console.log('Soy un loser');
    }
}

let jamie = new Luchador('Jamie', Familias.Lannister, 35, 'vivo');
let tyrion = new Asesor('Tyrion', Familias.Lannister, 40, 'vivo');
let bronn = new Escudero('Bronn', Familias.ASaber, 38, 'vivo');

tyrion.asesorado = jamie;
tyrion.comunicar();
jamie.comunicar();
bronn.comunicar();
