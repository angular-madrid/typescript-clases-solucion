var Familias;
(function (Familias) {
    Familias[Familias["Lannister"] = 0] = "Lannister";
    Familias[Familias["Targaryen"] = 1] = "Targaryen";
    Familias[Familias["ASaber"] = 2] = "ASaber";
})(Familias || (Familias = {}));
var Luchador = /** @class */ (function () {
    function Luchador(nombre, familia, edad, estado) {
        this.nombre = nombre;
        this.familia = familia;
        this.edad = edad;
        this.estado = estado;
    }
    Luchador.prototype.comunicar = function () {
        console.log("Primero pego y luego pregunto");
    };
    return Luchador;
}());
var Asesor = /** @class */ (function () {
    function Asesor(nombre, familia, edad, estado) {
        this.nombre = nombre;
        this.familia = familia;
        this.edad = edad;
        this.estado = estado;
    }
    Asesor.prototype.comunicar = function () {
        console.log('No sé por qué, pero creo que voy a morir pronto');
    };
    return Asesor;
}());
var Escudero = /** @class */ (function () {
    function Escudero(nombre, familia, edad, estado) {
        this.nombre = nombre;
        this.familia = familia;
        this.edad = edad;
        this.estado = estado;
    }
    Escudero.prototype.comunicar = function () {
        console.log('Soy un loser');
    };
    return Escudero;
}());
var jamie = new Luchador('Jamie', Familias.Lannister, 35, 'vivo');
var tyrion = new Asesor('Tyrion', Familias.Lannister, 40, 'vivo');
var bronn = new Escudero('Bronn', Familias.ASaber, 38, 'vivo');
tyrion.asesorado = jamie;
tyrion.comunicar();
jamie.comunicar();
bronn.comunicar();
//# sourceMappingURL=index.js.map